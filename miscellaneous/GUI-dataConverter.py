# An util to convert text splitted by space to numpy array or latex tabular
# created by Prof. Youjiang Wang
# On 2024-09-26

import sys
import os
rootPath = os.path.abspath(os.path.join(__file__, os.pardir, os.pardir))
sys.path.append(rootPath)

# pyinstaller.exe GUIAPP/GUI-dataConverter.py --onefile --noconsole

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class rootWindow(QMainWindow):
    def __init__(self, *args, **kargs):
        super().__init__(*args, **kargs)

        self.setGeometry(100,100,1000,800)
        self.setWindowTitle("数据转换(by交大有江)")
        self.create_main_layout()

    def create_main_layout(self):
        self.org_text = QPlainTextEdit()
        self.tgt_text = QTextEdit()
        self.tgt_text.setReadOnly(True)

        self.button_convert = QPushButton("转换numpy")
        self.button_latex = QPushButton("转换latex")
        self.button_paste = QPushButton("复制")
        self.button_convert.clicked.connect(self.convert_numpy)
        self.button_latex.clicked.connect(self.convert_latex)
        self.button_paste.clicked.connect(self.paste)

        boxb = QHBoxLayout()
        boxb.addWidget(self.button_convert)
        boxb.addWidget(self.button_latex)
        boxb.addWidget(self.button_paste)
        boxt = QHBoxLayout()
        boxt.addWidget(self.org_text)
        boxt.addWidget(self.tgt_text)
        
        form = QFormLayout()
        form.addRow(boxb)
        form.addRow(boxt)

        w = QWidget()
        w.setLayout(form)
        self.setCentralWidget(w)

    def convert_numpy(self):
        text = self.org_text.toPlainText()
        self.tgt_text.clear()
        arryMode = 1
        self.tgt_text.append("numpy.array([")
        for i, line in enumerate(text.splitlines()):
        # for i, line in enumerate(text.split('\n')):
            line = line.replace('\t', ' ').strip()
            if line:
                line = ', '.join(line.split())
                res = '    [' + line + '],'
                self.tgt_text.append(res)
                arryMode = 2
            else:
                if arryMode == 2:
                    self.tgt_text.append('])')
                    self.tgt_text.append("numpy.array([")
                    arryMode = 1
        
        self.tgt_text.append('])')

    def convert_latex(self):
        text = self.org_text.toPlainText()
        self.tgt_text.clear()
        for i, line in enumerate(text.splitlines()):
            line = line.replace('\t', ' ').strip()
            if line:
                line = ' & '.join(line.split()) + r' \\'
                self.tgt_text.append(line)

    def paste(self):
        self.tgt_text.selectAll()
        self.tgt_text.copy()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = rootWindow()
    ex.show()  # IMPORTANT!!!!! Windows are hidden by default.
    app.exec()