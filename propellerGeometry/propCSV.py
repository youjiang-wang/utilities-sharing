# Classes to define parameterized propeller geometry
# By Prof. Youjiang Wang
# 01/09/2024
import numpy
from enum import Enum
import scipy.interpolate as itp
import scipy.integrate as itg
import warnings
import copy

class sectionProfile():
    def __init__(self) -> None:
        self.__data = numpy.array(
            [],
            dtype = [('x', float), ('f', float), ('t', float), ('dfdx', float)]
            )

    @classmethod
    def buildArrayWithName(cls, size, camberName = "NACA a=0.8(Mod)", thickName = "NACA 66(Mod)"):
        res = []
        for i in range(size):
            ele = cls()
            ele.fromNames(camberName=camberName, thickName=thickName)
            res.append(ele)
        return res
    
    def copy(self):
        a = sectionProfile()
        a.__data = copy.deepcopy(self.__data)
        return a

    def fromNames(self, camberName = "NACA a=0.8(Mod)", thickName = "NACA 66(Mod)"):
        '''
        Returns
        ==========
        chord, maxCamber, maxThick 
        '''
        xrate = [0, .005, .0075, .0125, .025, .05, .075, .10, .15, .20, .25, .30, .35, .40, .45, .50, .55, .60,
                     .65, .70, .75, .80, .85, .90, .95, 1.0]

        if camberName == "NACA a=0.8":
            yrate = [.0000, .00287, .00404, .00616, .01077, .01841, .02483, .03043, .03985, .04748, .05367, .05863,
                        .06248, .06528, .06709, .06790, .06770, .06644, .06405, .06037, .05514, .04771, .03683, .02435,
                        .01163, .0000]
            tanf = [0, .48535, .44925, .40359, .34104, .27718, .23868, .21050, .16892, .13734, .11101, .08775,
                    .06634, .04601, .02613, .00620, -.01433, -.03611, -.06010, -.08790, -.12311, -.18412, -.23921,
                    -.25583, -.24904, -.20385]
        elif camberName == "NACA a=0.8(Mod)":
            yrate = [.0000, .00281, .00396, .00603, .01055, .01803, .02432, .02981, .03903, .04651, .05257, .05742,
                        .06120, .06394, .06571, .06651, .06631, .06508, .06274, .05913, .05401, .04673, .03607, .02452,
                        .01226, .0000]
            tanf = [0, .47539, .44004, .39531, .33404, .27149, .23378, .20618, .16546, .13452, .10873, .08595,
                    .06498, .04507, .02559, .00607, -.01404, -.03537, -.05887, -.08610, -.12058, -.18034, -.23430,
                    -.24521, -.24521, -.24521]
        elif camberName == "NACA a=1.0":
            yrate = [.0000, .00250, .00350, .00535, .00930, .01580, .02120, .02585, .03365, .03980, .04475, .04860,
                        .05150, .05355, .05475, .05515, .05475, .05355, .05150, .04860, .04475, .03980, .03365, .02585,
                        0.1580, .0000]
            tanf = [0, .42120, .38875, .34770, .29155, .23430, .19995, .17485, .13805, .11030, .08745, .06745,
                    .04925, .03225, .01595, .00000, -.01595, -.03225, -.04925, -.06745, -.08745, -.11030,
                    -.13805, -.17485, -.03430, -.00000]
        else:
            raise ValueError(f"Unsupported cmaberType:{camberName}.")
        
        if thickName == "NACA 66(Mod)":
            trate = [0, .0665, .0812, .1044, .1466, .2066, .2525, .2907, .3521, .4000, .4363, .4637, .4832, .4952,
                     .5000, .4962, .4846, .4653, .4383, .4035, .3612, .3110, .2532, .1877, .1143, .0333]
        else:
            raise ValueError(f"Unsupported thicknessType:{thickName}.")

        # maxf = max(yrate)
        # xc = numpy.array(xrate) * chord
        # yc = numpy.array(yrate)/maxf * (camber2c * chord)
        # yt = numpy.array(trate) * thick2c * chord # maxt is 0.5
        # realTanf = (numpy.array(tanf)/maxf * camber2c)
        # cosf = 1 / (realTanf **2 + 1) ** 0.5
        # sinf = cosf * realTanf
        # if normDir:
        #     xu = xc - yt*sinf
        #     yu = yc + yt*cosf
        #     xl = xc + yt*sinf
        #     yl = yc - yt*cosf

        return self.fromCamberThick(x = xrate, camber = yrate, thick = trate, camberSlope = tanf)
    
    def fromCamberThick(self, x, camber, thick, camberSlope = None):
        '''
        Returns
        ==========
        chord, maxCamber, maxThick 
        '''
        if camberSlope is None:
            camberSlope = numpy.zeros_like(camber)
        self.__data = numpy.array(
            [(xi, ci, ti, df) for xi, ci, ti, df in zip(x, camber, thick, camberSlope)],
            dtype = [('x', float), ('f', float), ('t', float), ('dfdx', float)]
            )
        return self._normalize()
    
    def fromBackFace(self, x, back, face):
        '''
        Returns
        ==========
        chord, maxCamber, maxThick 
        '''
        camber = (numpy.array(back) + numpy.array(face)) / 2.0
        thick = numpy.absolute(numpy.array(back) - numpy.array(face))
        return self.fromCamberThick(x, camber, thick)

    def _normalize(self):
        '''
        x from 0 to 1;
        max camber to 1.0;
        max thick to 0.5;

        Returns
        ==========
        chord, maxCamber, maxThick 
        '''
        if self.__data.shape[0] < 2:
            return 0, 0, 0
        x = self.__data['x']
        chord = (x[-1] - x[0])
        maxCamber = numpy.max(self.__data['f'])
        maxThick = numpy.max(self.__data['t'])
        self.__data['x'] = (x - x[0]) / chord
        self.__data['f'] /= maxCamber
        self.__data['t'] /= maxThick * 2.0
        self.__data['dfdx'] *= chord / maxCamber
        return chord, maxCamber, maxThick 
    
    @property
    def length(self):
        return self.__data.shape[0]
    
    @property
    def x(self):
        return copy.deepcopy(self.__data['x'])
    
    @property
    def camber(self):
        return copy.deepcopy(self.__data['f'])
    
    @camber.setter
    def camber(self, value, camberSlope = None):
        self.__data['f'] = copy.deepcopy(value)
        self._normalize()

    @property
    def camberSlope(self):
        return copy.deepcopy(self.__data['dfdx'])
    
    @property
    def halfThick(self):
        return copy.deepcopy(self.__data['t'])
    
    @halfThick.setter
    def halfThick(self, value):
        self.__data['t'] = copy.deepcopy(value)
        self._normalize()

    def separate2DCoord(self, chord, t2c, f2c, xPos = None):
        '''
        xPos is from 0 to 1
        '''
        x = self.x
        camber = self.camber
        thick = self.halfThick

        if xPos is not None:
            camber = itp.interp1d(x, camber, kind='cubic')(xPos)
            thick = itp.interp1d(x, thick, kind='cubic')(xPos)
            x = xPos

        x *= chord
        camber *= f2c * chord
        thick *= t2c * chord
        back = (camber + thick)
        face = (camber - thick)
        return x, back, face

    def continous2DCoord(self, chord, t2c, f2c, xPos = None):
        #TODO: consider camberSlope for large camber, e.g. pre-stator
        x, back, face = self.separate2DCoord(chord = chord, t2c = t2c, f2c = f2c, xPos = xPos)
        x2d = numpy.array(x.tolist()[::-1] + x.tolist()[1:])
        y2d = numpy.array(back.tolist()[::-1] + face.tolist()[1:])
        return x2d, y2d

    def enlargeTEThick(self, te2maxThick):
        '''
        only increase, will not decrease.
        '''
        if self.__data.shape[0] < 2:
            return
        self._normalize()
        maxID = numpy.argmax(self.__data['t'])
        maxT = self.__data['t'][maxID]
        old_te2maxT = self.__data['t'][-1] / maxT
        if old_te2maxT >= te2maxThick:
            return
        
        self.__enlargeTE_pro2(maxID, maxT, old_te2maxT, te2maxThick)

    def __enlargeTE_pro1(self, maxID, maxT, old_te2maxT, te2maxThick):
        '''
        increase thickness proportionaly, 1-order smothness near the 
        '''
        if old_te2maxT >= te2maxThick:
            return
        
        proportionalCoef = (maxT - te2maxThick) / (maxT - old_te2maxT)
        self.__data['t'][maxID:] = maxT - (maxT - self.__data['t'][maxID:]) * proportionalCoef

    def __enlargeTE_pro2(self, maxID, maxT, old_te2maxT, te2maxThick):
        '''
        a method to ensure the countinouity of second order derivative
        '''
        if old_te2maxT >= te2maxThick:
            return
        
        h_pre = self.x[maxID] - self.x[maxID-1]
        h_next = self.x[maxID+1] - self.x[maxID]
        dy_pre = self.halfThick[maxID] - self.halfThick[maxID-1]
        dy_next = self.halfThick[maxID] - self.halfThick[maxID+1]
        f20 = (h_pre * dy_next + h_next * dy_pre) / (h_pre * h_next * (h_pre + h_next) * 0.5) # ddy/dx
        # dy / dx == 0
        x0 = self.x[maxID]
        y0 = maxT

        base = y0 + 0.5 * (self.x[maxID:] - x0)**2 * f20
        rest = self.halfThick[maxID:] - base
        proportionalCoef = (maxT * te2maxThick - base[-1]) / rest[-1]
        self.__data['t'][maxID:] = base + proportionalCoef * rest

class paraBladeGeometry():
    class skewTypeEnum(Enum):
        skewInDeg = 0
        distLE2D = 1

    def __init__(self):
        self.__data = numpy.array(
            [],
             dtype = [('r2R', float), ('chord2D', float), ('pitch2D', float), 
                      ('thick2c', float), ('camber2c', float), ('rake2D', float), 
                      ('skewInDeg', float), ('distLE2D', float), ('profile', sectionProfile)]
            )

    def setParameter(self, r2R, chord2D, pitch2D, thick2c, camber2c,
        rake2D, profile, Diameter, skewType, skewInDeg = None, distLE2D = None,
        righthand = True, bladeNumber = 1):
        '''
        skewType : one of 'skewInDeg', 'distLE2D'
        '''

        if skewType == 'skewInDeg':
            self.__skewType = self.skewTypeEnum.skewInDeg
        elif skewType == 'distLE2D':
            self.__skewType = self.skewTypeEnum.distLE2D
        else:
            raise ValueError('skewType must be one of (skewInDeg, distLE2D)')
        
        if self.__skewType == self.skewTypeEnum.skewInDeg:
            self._checkSize([r2R, chord2D, pitch2D, thick2c, camber2c,
                rake2D, skewInDeg])
            distLE2D = numpy.zeros_like(r2R)
        elif self.__skewType == self.skewTypeEnum.distLE2D:
            self._checkSize([r2R, chord2D, pitch2D, thick2c, camber2c,
                rake2D, distLE2D])
            skewInDeg = numpy.zeros_like(r2R)
        else:
            raise RuntimeError(f'Unrecoginzed skewType {self.__skewType}')
        
        self.__data = numpy.array(
            [(r, c, p, t, f, x, s, d, sec) for r, c, p, t, f, x, s, d, sec in 
             zip(r2R, chord2D, pitch2D, thick2c, camber2c, rake2D, skewInDeg, distLE2D, profile)], 
             dtype = [('r2R', float), ('chord2D', float), ('pitch2D', float), 
                      ('thick2c', float), ('camber2c', float), ('rake2D', float), 
                      ('skewInDeg', float), ('distLE2D', float), ('profile', sectionProfile)]
            )
        
        self.Diameter = Diameter
        self.righthand = righthand
        self.bladeNumber = bladeNumber
        # self.__radiiNumber = self.__data.shape[0]
        
    def _checkSize(self, arrayList):
        len0 = len(arrayList[0])
        for a in arrayList[1:]:
            if len(a) != len0:
                raise ValueError('Lengths of input data must be the same!')
    
    def adjustMinTeThick(self, minTeThick):
        if numpy.isscalar(minTeThick):
            minTeThick = minTeThick * numpy.ones_like(self.radiiNumber)
        assert(len(minTeThick) == self.radiiNumber)
        for ir in range(self.radiiNumber):
            te2maxThick = minTeThick[ir] / (self.Diameter * self.chord2D[ir] * self.thick2c[ir] + 1e-9)
            self.profile[ir].enlargeTEThick(te2maxThick = te2maxThick)

    def get2DSectionCoord(self, secID:int):
        assert(secID >= 0 and secID < self.radiiNumber)
        chord = self.chord2D[secID] * self.Diameter
        xx, back, face = self.profile[secID].separate2DCoord(chord, self.thick2c[secID], self.camber2c[secID])
        return xx, back, face
    
    def completeLastProfile(self):
        '''
        Generate a profile for the last section if it is empty.
        This is needed when generating 3D coordinate.
        '''
        if self.profile[-1].length == 0:
            self.profile[-1] = self.profile[-2].copy()

    @property
    def radiiNumber(self):
        return self.__data.shape[0]

    @property
    def r2R(self):
        return self.__data['r2R']
   

    @property
    def chord2D(self):
        return self.__data['chord2D']

    @chord2D.setter
    def chord2D(self, value):
        self.__data['chord2D'] = value


    @property
    def pitch2D(self):
        return self.__data['pitch2D']

    @pitch2D.setter
    def pitch2D(self, value):
        self.__data['pitch2D'] = value


    @property
    def thick2c(self):
        return self.__data['thick2c']

    @thick2c.setter
    def thick2c(self, value):
        self.__data['thick2c'] = value


    @property
    def camber2c(self):
        return self.__data['camber2c']

    @camber2c.setter
    def camber2c(self, value):
        self.__data['camber2c'] = value


    @property
    def rake2D(self):
        return self.__data['rake2D']

    @rake2D.setter
    def rake2D(self, value):
        self.__data['rake2D'] = value

    @property
    def profile(self):
        return self.__data['profile']
    
    @profile.setter
    def profile(self, value):
        self.__data['profile'] = value

    @property
    def skewInDeg(self):
        if self.__skewType == self.skewTypeEnum.skewInDeg:
            return self.__data['skewInDeg']
        else:
            cotTheta = (self.r2R * numpy.pi) / (self.pitch2D**2 + (self.r2R * numpy.pi)**2 )**0.5
            skew = numpy.rad2deg((self.chord2D * 0.5 - self.distLE2D) * cotTheta * 2.0 / self.r2R)
            return skew

    @skewInDeg.setter
    def skewInDeg(self, value):
        if self.__skewType == self.skewTypeEnum.skewInDeg:
            self.__data['skewInDeg'] = value
        else:
            raise RuntimeError(f'Cannot set skewInDeg when the skewType is {self.__skewType}')


    @property
    def distLE2D(self):
        if self.__skewType == self.skewTypeEnum.distLE2D:
            return self.__data['distLE2D']
        else:
            cotTheta = (self.r2R * numpy.pi) / (self.pitch2D**2 + (self.r2R * numpy.pi)**2 )**0.5
            dist = self.chord2D * 0.5 - numpy.radians(self.skewInDeg) * self.r2R / 2.0 / cotTheta
            return dist

    @distLE2D.setter
    def distLE2D(self, value):
        if self.__skewType == self.skewTypeEnum.distLE2D:
            self.__data['distLE2D'] = value
        else:
            raise RuntimeError(f'Cannot set distLE2D when the skewType is {self.__skewType}')

    @property
    def skewType(self):
        return self.__skewType.name
    
    @skewType.setter
    def skewType(self, str):
        newType = self.skewTypeEnum[str]
        if newType == self.__skewType:
            pass
        elif (newType == self.skewTypeEnum.skewInDeg) and (self.__skewType == self.skewTypeEnum.distLE2D):
            self.__data['skewInDeg'] = self.skewInDeg
            self.__skewType = newType
        elif (newType == self.skewTypeEnum.distLE2D) and (self.__skewType == self.skewTypeEnum.skewInDeg):
            self.__data['distLE2D'] = self.distLE2D
            self.__skewType = newType
        else:
            raise NotImplementedError(f'Transfer from {self.__skewType} to {newType} is not implemented.')
        
    @property
    def AeA0(self):
        return itg.simpson(y = self.chord2D, x = self.r2R) * self.bladeNumber * 2.0 / numpy.pi
    
    @property
    def rakeInDeg(self):
        return numpy.rad2deg(numpy.arctan(self.rake2D[-1] * 2.0))
    
    @property
    def pitchRatio07(self):
        return itp.interp1d(self.r2R, self.pitch2D)(0.7)

class fileIO():
    @classmethod
    def loadPropCSV(cls, filePath):
        section = 0
        secline = 0
        version = 1.0
        parameterData = []
        profileData = []
        nonEmptyCount = []
    
        with open(filePath, mode = 'r', encoding="ANSI") as file:
            for line in file:
                cells = line.split(',')
                if section == 0 and cells[0].strip().startswith('##') :
                    # version line
                    version = float(cells[2])
                elif section == 0 and cells[0].strip().startswith("***"):
                    # begining of metainfo section
                    section = 1
                    secline = 0
                elif section == 1 and secline < 3:
                    secline += 1
                elif section == 1 and secline == 3:
                    secline += 1
                    diameter = float(cells[0])
                    bladeNum = int(cells[1])
                    righthand = (cells[4].strip() == "右")
                elif section == 1 and secline > 3 and cells[0].strip().startswith("***"):
                    # end of metainfo section
                    # begin header in next line
                    section = 2
                    secline = 0
                elif section == 1:
                    raise RuntimeError(f'Unexpected metainfo {secline}-th line: {line}')
                elif section == 2 and secline < 1:
                    secline += 1
                elif section == 2 and secline == 1:
                    secline += 1
                    # symbol line in the header
                    skewType = cells[6].strip()
                    if skewType == "le2ref":
                        pass
                    elif skewType == "skew":
                        pass
                    else:
                        raise RuntimeError(f"Unexpected skew symbol {skewType}, must be 'le2ref' or 'skew'.")
                elif section == 2 and secline < 2:
                    secline += 1
                elif section == 2 and secline == 2:
                    # 4 lines of header is over, section profiles begin
                    section = 3
                    secline = 0
                elif section == 3 and secline == 0:
                    nonEmptyCount = 0
                    if cells[0].strip() != '':
                        nonEmptyCount = 1
                        parameterData.append(list(map(float, cells[:8])))
                        if ''.join(cells[9:]).strip() == '':
                            # an empty line means the empty tip section
                            profileData.append([[], [], []])
                            break

                    profileX = list(map(float, [str for str in cells[9:] if str.strip()]))
                    secline += 1
                elif section == 3:
                    if cells[0].strip() != '':
                        if nonEmptyCount == 1:
                            raise RuntimeError(f"More than one non-empty section data line at {line}")
                        nonEmptyCount = 1
                        parameterData.append(list(map(float, cells[:8])))
                        if ''.join(cells[9:]).strip() == '':
                            # an empty line means the empty tip section
                            profileData.append([[], [], []])
                            break
                    
                    if secline == 1:
                        profileBack = list(map(float, [str for str in cells[9:] if str.strip()]))
                    elif secline == 2:
                        profileFace = list(map(float, [str for str in cells[9:] if str.strip()]))
                        profileData.append([profileX, profileBack, profileFace])
                        if nonEmptyCount == 0:
                            raise RuntimeError(f"Non section data at {line}")
                    secline = (secline + 1) % 3
    
        # convert loaded data to other things
        if (not parameterData) or (not profileData):
            warnings.warn("No data has been read")
            return None
        
        assert(len(parameterData) == len(profileData))
        
        chord = []
        f2c = []
        t2c = []
        profiles = []
        for i, pd in enumerate(profileData):
            sp = sectionProfile()
            _, maxCamber, maxThick = sp.fromBackFace(pd[0], pd[1], pd[2])
            if pd[0]:
                chord.append(pd[0][-1] - pd[0][0])
                f2c.append(maxCamber / chord[-1])
                t2c.append(maxThick / chord[-1])
            else:
                chord.append(parameterData[i][3])
                f2c.append(parameterData[i][4] / (chord[-1] + 1e-9))
                t2c.append(parameterData[i][5] / (chord[-1] + 1e-9))
            profiles.append(sp)
        
        blade = paraBladeGeometry()
        parameterData = numpy.array(parameterData)
        blade.setParameter(
            r2R = parameterData[:,0],
            chord2D = numpy.array(chord) / diameter, 
            pitch2D = parameterData[:,2] / diameter,
            thick2c = t2c,
            camber2c = f2c,
            rake2D = parameterData[:,7] / diameter,
            profile = profiles, 
            Diameter = diameter, 
            skewType = ('skewInDeg' if skewType == 'skew' else 'distLE2D'),
            skewInDeg = (parameterData[:,6] if skewType == 'skew' else None), 
            distLE2D = (parameterData[:,6]/diameter if skewType == 'le2ref' else None),
            righthand = righthand,
            bladeNumber = bladeNum
            )
        return blade

    @classmethod
    def savePropCSV(cls, paraBlade:paraBladeGeometry, filePath:str, skewType = None):
        '''
        skewType: 
        one of None / 'skewInDeg' / 'distLE2D' 
        None means use the type in paraBlade
        '''
        if skewType is None:
            skewType = paraBlade.skewType
        if skewType == "skewInDeg":
            skewHeader = ['侧斜', 'skew', '[角度]']
            skewValue = paraBlade.skewInDeg
        elif skewType == "distLE2D":
            skewHeader = ['导边至中心', 'le2ref', '[mm]']
            skewValue = paraBlade.distLE2D * paraBlade.Diameter

        with open(filePath, 'w', encoding='ANSI') as file:
            file.write("##, 版本=, 1.0\n 易读改桨叶几何定义文件\n 格式支持:, 上海交大, 王有江, youjiang.wang@sjtu.edu.cn\n")
            file.write("***\n")
            file.write("直径, 叶数, 螺距比, 盘面比, 旋向, 侧斜角, 纵倾角\n D, Z, P/D, Ae/A0, , skew, \n [mm],,,,,[角度],[角度]\n")
            file.write(f"{paraBlade.Diameter},{paraBlade.bladeNumber},{paraBlade.pitchRatio07:>.4f}," +
                       f"{paraBlade.AeA0:>.4f},{'右' if paraBlade.righthand else '左'},{paraBlade.skewInDeg[-1]:>.2f},{paraBlade.rakeInDeg:>.2f}\n")
            file.write("***\n")

            file.write(f"无量纲半径, 半径, 螺距, 弦长, 最大厚度, 最大拱度, {skewHeader[0]}, 纵倾\n")
            file.write(f"r/R, r, P, c, t, f, {skewHeader[1]}, rake\n")
            file.write(f", [mm], [mm], [mm], [mm], [mm], {skewHeader[2]}, [mm]\n")

            D = paraBlade.Diameter
            r2R = paraBlade.r2R
            radii = r2R * D / 2.0
            pitch = paraBlade.pitch2D * D
            chord = paraBlade.chord2D * D
            thick = paraBlade.thick2c * chord
            camber = paraBlade.camber2c * chord
            rake = paraBlade.rake2D * D
            for ir in range(paraBlade.radiiNumber):
                file.write("{:>.3f},{:>.2f},{:>.2f},{:>.2f},{:>.2f},{:>.2f},{:>.2f},{:>.2f},".format(
                    r2R[ir], radii[ir], pitch[ir], chord[ir], thick[ir], camber[ir], skewValue[ir], rake[ir]))
                xx, back, face = paraBlade.get2DSectionCoord(ir)
                file.write("X," + ','.join(['{:>.2f}'.format(x) for x in xx]) + '\n')
                file.write(","*8 + "叶背," + ','.join(['{:>.2f}'.format(y) for y in back]) + '\n')
                file.write(","*8 + "叶面," + ','.join(['{:>.2f}'.format(y) for y in face]) + '\n')

if __name__ == "__main__":
    file = r"E:\document\test1.csv"
    blade = fileIO.loadPropCSV(file)
    fileIO.savePropCSV(blade, r"E:\document\test2.csv", skewType="skewInDeg")