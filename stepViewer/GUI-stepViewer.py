#!/usr/bin/env python
# created by Prof. Youjiang Wang on 2024-05-05
# pyinstaller.exe GUIAPP/GUI-stepViewer.py --onefile --noconsole --add-data D:/ProgramData/anaconda3/envs/env_python311/Library/share/opencascade:casroot

import os
import sys

# this section is the modification needed to create an executable using pyinstaller provided by Waterbug:
#  if we are running on Windows and pyinstaller installed us, there will
#  be a 'casroot' directory that contains files needed by pythonocc --
#  copy them to home and set "CASROOT" env var ...
import sys
app_module_path = 'app_module'
if sys.platform == 'win32':
    casroot_path = 'casroot'
    if os.path.exists(casroot_path):
        os.environ['CASROOT'] = casroot_path
#end modification

from OCC.Core.STEPControl import STEPControl_Reader

from PyQt5.QtWidgets import (
    QApplication,
    QPushButton,
    QHBoxLayout,
    QDialog,
    QVBoxLayout,
    QFileDialog,
    QMessageBox,
)

from OCC.Display.backend import load_backend
load_backend("pyqt5")
import OCC.Display.qtDisplay as qtDisplay

class App(QDialog):
    def __init__(self):
        super().__init__()
        self.title = "STEP Viewer (by 交大有江)"
        self.left = 300
        self.top = 100
        self.width = 900
        self.height = 700
        self.ais_step = []
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        windowLayout = QVBoxLayout()

        self.canvas = qtDisplay.qtViewer3d(self)
        windowLayout.addWidget(self.canvas)
        self.createButtonLayout()
        windowLayout.addLayout(self.buttonLayout)

        self.setLayout(windowLayout)
        self.show()
        self.canvas.InitDriver()
        self.canvas.resize(600, 600)
        self.display = self.canvas._display

    def createButtonLayout(self):
        layout = QHBoxLayout()

        disp = QPushButton("Open STP File", self)
        disp.clicked.connect(self.chooseStep)
        layout.addWidget(disp)

        eras = QPushButton("Delete Last", self)
        eras.clicked.connect(self.deleteLast)
        layout.addWidget(eras)

        clr = QPushButton("Clear All", self)
        clr.clicked.connect(self.clearAll)
        layout.addWidget(clr)

        self.buttonLayout = layout

    def chooseStep(self):
        file_path, _ = QFileDialog.getOpenFileName(
                    caption = "Open STEP File",
                    directory = "",
                    filter = "STP File(*.stp);;STEP File(*.step);;All Files(*.*)")
        if file_path == "":
            return

        try:
            step_reader = STEPControl_Reader()
            step_reader.ReadFile(file_path)
            step_reader.TransferRoot()
            myshape = step_reader.Shape()
            self.ais_step.append(self.display.DisplayShape(myshape)[0])
            self.display.FitAll()
        except Exception as e:
            QMessageBox.critical(self, "Error", str(e))

    def deleteLast(self):
        if len(self.ais_step) > 0:
            self.display.Context.Erase(self.ais_step.pop(), True)

    def clearAll(self):
        while len(self.ais_step) > 0:
            self.display.Context.Erase(self.ais_step.pop(), True)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = App()
    if os.getenv("APPVEYOR") is None:
        sys.exit(app.exec_())