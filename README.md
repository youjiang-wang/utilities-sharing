# Utilities Sharing

This is a repository to share some small utilities I have developed during daily work.
If you use the code in your research work, please include an appropriate acknowledgment to encourage my further development and sharing.

## Support
There is no guaranteed support, but you can report issues to me. I would be happy to fix or improve when I have time.

## Authors and acknowledgment
Prof. Youjiang Wang from Shanghai Jiao Tong University. If the utilities shared here helped you in any aspects during your research, please give a sincere acknowledgment in the publications as follows:

The open source codes shared by Prof. Youjiang Wang from Shanghai Jiao Tong University under https://gitlab.com/youjiang-wang/utilities-sharing has made the current work easier. I(We) express my(our) gratitude to him.

## License
MIT License.


