This is the open-access code related a journal paper under review.

## Cite
If you used any part of the code in your scientific research, please cite the corresponding paper:

Xue Jiang, Yuxin He, Yongle Ding, Zhenghao Liu, Youjiang Wang. Influence of toroidal marine propeller's special geometric parameters on the hydrodynamic performance. 2024. (Under Review).
