import numpy as np
import scipy.integrate as itg
import scipy.interpolate as itp
import math

class toroidalBladeGeometry():
    '''
    Example
    ----------
    tb = toroidalBladeGeometry()
    r2R, xm2D, thetam, gamma, phi, chord2D, pitch2D, thick2c, camber2c, rake2D, skew = tb.getAnExample()

    # pithing and roll angle, gammaTip is the pitching angle at the blade tip (in degrees)
    # influenceRange is the range of R that there will be pitching and roll angle
    tb.calculatePitchingAndRoll(r2R, xm2D, thetam, pitch2D, gammaTip = 10, influenceRange=0.1)

    tb.fromDesignParameters(r2R, xm2D, thetam, gamma, phi, chord2D, pitch2D, thick2c, camber2c, rake2D, skew)
    tb.visualizeBlade()
    # tb.saveProfileNX("../Data/torBlade.dat")
    '''
    def __init__(self):
        self.Xp = []
        self.Yp = []
        self.Zp = []

    def fromDesignParameters(self, r2R, xm2D, thetam, gamma, phi, chord2D,
            pitch2D, thick2c, camber2c, D:float=1.0):
        '''
        Generate 3D coordinates from sectional parameters

        Parameters
        ----------
        r2R : array_like, shape (n,)
            1-D array containing values of the radius ratio, should between 0 and 1
        xm2D : array_like, shape (n,)
            1-D array containing values of the x coordinate of blade reference line,
            nondimensionalized with diameter
        thetam : array_like, shape (n,)
            1-D array containing values of the skew angle of blade reference line,
            in degrees
        gamma : array_like, shape (n,)
            1-D array containing values of the pitching angle of blade sections,
            in degrees
        phi : array_like, shape (n,)
            1-D array containing values of the rolling angle of blade sections,
            in degrees
        chord2D : array_like, shape (n,)
            1-D array containing values of the chord length, nondimensionalized
            with diameter
        pitch2D : array_like, shape (n,)
            1-D array containing values of the pitch ratio
        thick2c : array_like, shape (n,)
            1-D array containing values of the section maximum thickness,
            nondimensionalized with chordlength
        camber2c : array_like, shape (n,)
            1-D array containing values of the section maximum camber,
            nondimensionalized with chordlength
        rake2D : array_like, shape (n,), optional
            1-D array containing values of the rake,
            nondimensionalized with diameter.
            Default is None, which means all are zero.
        skew : array_like, shape (n,), optional
            1-D array containing values of the section maximum camber,
            in degrees.
            Default is None, which means all are zero.
        D : float, optional
            The diameter of propeller. Default is 1.0
        '''
        # section profile: a=0.8(mod) camber line & naca66(mod) thickness
        s_c = np.array([
              0,    .005, .0075, .0125,  .025,  .05,   .075,   .10,   .15,
            .20,     .25,   .30,   .35,   .40,  .45,    .50,   .55,   .60,
            .65,     .70,   .75,   .80,   .85,  .90,    .95,  .975,   1.0])
        y_c = np.array([
            .0000, .0423, .0595, .0907, .1586, .2712, .3657, .4482,  .5869,
            .6993, .7905, .8635, .9202, .9615, .9881, 1.0000, .9971, .9786,
            .9434, .8892, .8121, .7027, .5425, .3586,  .1713, .0823, 0])
        y_t = np.array([
            .0000, .0665, .0812, .1044, .1466, .2066, .2525, .2907, .3521,
            .4000, .4363, .4637, .4832, .4952, .5000, .4962, .4846, .4653,
            .4383, .4035, .3612, .3110, .2532, .1877, .1143, .0748, .0333])

        Xp = []
        Yp = []
        Zp = []

        tipID = None
        for i in range(1, len(r2R)):
            if abs(r2R[i] - 1.0) < 1e-4:
                tipID = i
        if (tipID is None):
            raise ValueError("Fail to find tipID where r/R = 1.0.")

        def pitch2DFunc(rRatio, isFront):
            if isFront:
                return np.interp(rRatio, r2R[:tipID+1], pitch2D[:tipID+1])
            else:
                return np.interp(rRatio, r2R[:tipID:-1], pitch2D[:tipID:-1])

        for i in range(len(r2R)):
            radius = r2R[i] * D * 0.5
            chord = chord2D[i] * D
            camber = camber2c[i] * chord
            thickness = thick2c[i] * chord

            # 1. get the 2D section coordinates
            x1 = s_c * chord
            x1 = np.hstack((x1[::-1], x1[1:]))
            yup = camber * y_c + y_t * thickness
            ylow = camber * y_c - y_t * thickness
            y1 = np.hstack((ylow[::-1], yup[1:]))

            x1 = 0.5 * chord - x1

            # 2. rotate around x axis by roll angle
            cosPhi = math.cos(math.radians(phi[i]))
            sinPhi = math.sin(math.radians(phi[i]))
            z1 = y1 * sinPhi
            y1 = y1 * cosPhi

            #3. pitching around y axis by pitching angle
            cosGamma = math.cos(math.radians(gamma[i]))
            sinGamma = math.sin(math.radians(gamma[i]))
            x2 = x1*cosGamma - z1*sinGamma
            y2 = y1
            z2 = x1*sinGamma + z1*cosGamma

            #4. mapping to cylinderical surface
            r = radius + z2
            tanBeta = pitch2DFunc(r*2/D, i<=tipID) * D / (2 * math.pi * r) 
            cosBeta = 1.0 / (1 + tanBeta**2)**0.5
            sinBeta = tanBeta * cosBeta

            x = x2*sinBeta + y2*cosBeta
            theta = (x2*cosBeta - y2*sinBeta)/r
            x += xm2D[i] * D 
            theta += np.radians(thetam[i])

            # cylindrical coordinate to cartesian coordinate
            y = r * np.cos(theta)
            z = r * np.sin(theta)
            
            Xp.append(x)
            Yp.append(y)
            Zp.append(z)

        self.Xp = Xp
        self.Yp = Yp
        self.Zp = Zp

    def getAnExample(self, gammaTip = 0):
        '''
        Get an arbitray example for toroidal blade geometry
        '''
        r2R = np.array([0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.92, 0.94, 0.96, 0.98, 0.99, 0.998, 1.0])
        xm2D = (1 - ((r2R - 0.2)/0.8)**2)**0.5 * 0.1
        thetam = (1 - ((r2R - 0.2)/0.8)**2)**0.5 * 10

        # ka chord length distribution
        r_ka   = np.arange(0.2, 1.1, 0.1) # radius ratio
        ch_ka = np.array([67.15, 76.59, 85.19, 93.01, 100,
                105.86, 110.08, 112.66, 112.88])/100;
        # ch6 = 1.969/Z*AeA0; # 0.6R处弦长，单位同D

        chord2D = 0.2 * np.interp(r2R, r_ka, ch_ka)
        pitch2D = 1.0 * np.ones_like(r2R)
        thick2c = 0.2 * (1-r2R) + 0.03 * r2R
        camber2c = 0.5 * thick2c

        # make a symmetric distribution
        r2R = np.hstack((r2R, r2R[-2::-1]))
        xm2D = np.hstack((xm2D, -xm2D[-2::-1]))
        thetam = np.hstack((thetam, -thetam[-2::-1]))
        chord2D = np.hstack((chord2D, chord2D[-2::-1]))
        pitch2D = np.hstack((pitch2D, pitch2D[-2::-1]))
        thick2c = np.hstack((thick2c, thick2c[-2::-1]))
        camber2c = np.hstack((camber2c, camber2c[-2::-1]))

        gamma, phi = self.calculatePitchingAndRoll(r2R, xm2D, thetam, pitch2D, gammaTip = gammaTip)

        rake2D = None
        skew = None

        return r2R, xm2D, thetam, gamma, phi, chord2D, pitch2D, thick2c, camber2c, rake2D, skew

    @staticmethod
    def calculatePitchingAndRoll(r2R, xm2D, thetam, pitch2D, gammaTip, influenceRange = 0.1):
        '''
        Assume diameter is 1, thus r = r2R * 0.5;

        Parameters
        ----------
        r2R : array_like, shape (n,)
            1-D array containing values of the radius ratio, should between 0 and 1
        xm2D : array_like, shape (n,)
            1-D array containing values of the x coordinate of blade reference line,
            nondimensionalized with diameter
        thetam : array_like, shape (n,)
            1-D array containing values of the skew angle of blade reference line,
            in degrees
        pitch2D : array_like, shape (n,)
            1-D array containing values of the pitch ratio
        gammaTip : float
            The pitching angle at blade tip, in degrees
        influenceRange : float
            The radius range around the tip where there will be nonzero pitching and rolling angles

        Returns
        ----------
        gamma: array_like, shape (n,)
            pitching angles for each blade section
        phi: array_like, shape (n,)
            rolling angles for each blade section
        '''
        def calculatePhi(r, thetam, r2s, theta2s, x2s, gamma, pitch):
            '''
            Calculate the roll angle

            All angle in inputs and outpus are in degrees
            '''
            beta = math.atan(pitch / (2 * math.pi * r))
            gamma = math.radians(gamma)
            thetam = math.radians(thetam)
            theta2s = math.radians(theta2s)

            # in cylindrical coordinate, (r, theta, x)
            n1 = [math.cos(gamma), -math.sin(gamma)*math.cos(beta)/r, -math.sin(gamma)*math.sin(beta)]
            n2 = [r2s, theta2s, x2s]
            tnt = [math.sin(gamma), math.cos(gamma)*math.cos(beta)/r, math.cos(gamma)*math.sin(beta)] # direction of nose-tail line

            # to cartesian coordinate
            n1c = [n1[2], n1[0]*math.cos(thetam) - n1[1]*r*math.sin(thetam), n1[0]*math.sin(thetam) + n1[1]*r*math.cos(thetam)]
            n2c = [n2[2], n2[0]*math.cos(thetam) - n2[1]*r*math.sin(thetam), n2[0]*math.sin(thetam) + n2[1]*r*math.cos(thetam)]
            tntc = [tnt[2], tnt[0]*math.cos(thetam) - tnt[1]*r*math.sin(thetam), tnt[0]*math.sin(thetam) + tnt[1]*r*math.cos(thetam)]

            tempX = np.array(n1c)
            tempY = np.cross(tntc, n1c)
            tempX /= np.linalg.norm(tempX)
            tempY /= np.linalg.norm(tempY)
            phiRad = math.atan2(np.dot(n2c, tempY), np.dot(n2c, tempX))
            phiDeg = math.degrees(phiRad)
            return phiDeg

        tipID = None
        s = [0]
        for i in range(1, len(r2R)):
            ds = ((r2R[i] - r2R[i-1])**2 * 0.25 + (xm2D[i] - xm2D[i-1])**2)**0.5
            s.append(ds + s[-1])
            if abs(r2R[i] - 1.0) < 1e-4:
                tipID = i
        s = np.array(s)
        # s[:tipID] /= (2.0 * s[tipID])
        # s[tipID:] = 0.5 + 0.5 * (s[tipID:] - s[tipID]) / (s[-1] - s[tipID])
        sTip = s[tipID]

        # TODO: numerical differencing & tPhi
        x2s = []
        r2s = []
        theta2s = []
        for i in range(len(r2R)):
            if i == 0:
                x2s.append((xm2D[1] - xm2D[0]) / (s[1] - s[0]))
                r2s.append(0.5 * (r2R[1] - r2R[0]) / (s[1] - s[0]))
                theta2s.append((thetam[1] - thetam[0]) / (s[1] - s[0]))
            elif (i == len(r2R) - 1):
                ds =  (s[-1] - s[-2])
                x2s.append((xm2D[-1] - xm2D[-2]) / ds)
                r2s.append(0.5 * (r2R[-1] - r2R[-2]) / ds)
                theta2s.append((thetam[-1] - thetam[-2]) / ds)
            else:
                ds1 = s[i] - s[i-1]
                ds2 = s[i+1] - s[i]
                func = lambda y: (ds1**2 * y[2] - ds2**2 * y[0] - (ds1**2 - ds2**2) * y[1]) / (ds1**2 * ds2 + ds2**2 * ds1)
                x2s.append(func(xm2D[i-1:i+2]))
                r2s.append(0.5 * func(r2R[i-1:i+2]))
                theta2s.append(func(thetam[i-1:i+2]))

        rCrt = 1 - influenceRange
        s1 = np.interp(rCrt, r2R[:tipID], s[:tipID])
        s2 = np.interp(rCrt, r2R[:tipID:-1], s[:tipID:-1])

        pitching = np.zeros_like(r2R)
        phi = np.zeros_like(r2R)

        for i, ss in enumerate(s):
            if ss > s1 and ss <= sTip:
                t = 1 - (sTip - ss) / (sTip - s1)
                pitching[i] = gammaTip * (math.sin((t-0.5)*math.pi) + 1) / 2
                tPhi = calculatePhi(r2R[i] * 0.5, thetam[i], r2s[i], theta2s[i], x2s[i], pitching[i], pitch2D[i])
                phi[i] = tPhi * t
            elif ss >= sTip and ss < s2:
                t = 1 - (ss - sTip) / (s2 - sTip)
                pitching[i] = gammaTip * (math.sin((t-0.5)*math.pi) + 1) / 2
                tPhi = calculatePhi(r2R[i] * 0.5, thetam[i], r2s[i], theta2s[i], x2s[i], pitching[i], pitch2D[i])
                phi[i] = tPhi * t + 180 * (1-t)
            elif ss >= s2:
                phi[i] = 180

        gamma = pitching
        return gamma, phi

class paperGeom:
    def __init__(self) -> None:
        self.__xg = []
        self.__yg = []
        self.__zg = []
        self.__bg = None

    @classmethod
    def outline_modify(cls, ktip = 0.8, kall = 0.86):
        '''
        modification of 438x outline

        Parameters
        ----------
        ktip: coefficient to modify the outline between 0.7R and tip
        kall: coefficient to modify the whole outline proportionally (to adjust area ratio)
        '''
        def calcAeA0(dat, Z):
            AeA0 = itg.simpson(2*dat[:,1], dat[:,0], even='simpson') / np.pi
            return Z*AeA0

        data = np.array([
            [0.2, 0.174],
            [0.25, 0.202],
            [0.3, 0.229],
            [0.4, 0.275],
            [0.5, 0.312],
            [0.6, 0.337],
            [0.7, 0.347],
            [0.8, 0.334],
            [0.9, 0.28],
            [0.95, 0.21],
            [1, 0.001]])

        # print("Original Ae/A0(Z=5):", calcAeA0(data, 5))

        newC = np.copy(data)
        newC[data[:,0]>0.7, 1] = ktip * 0.347 + (1-ktip) * data[data[:,0]>0.7, 1]
        newC[:, 1] = newC[:, 1] * kall

        # print(f"New Ae/A0 (ktip={ktip}, kall={kall}, Z=6):", calcAeA0(newC, 6))

        return newC

    @classmethod
    def combineParameter(cls, ktip = 0.8, kall = 0.86, gammaTip = 0, pitchFrontK = 1, pitchRearK = 1, xdis = 0.1, angledif=36):
        '''
        combine 4382 and 4381 to toroidal propeller

        Parameters
        ----------
        ktip: coefficient to modify the outline between 0.7R and tip
        kall: coefficient to modify the whole outline proportionally (to adjust area ratio)
        gammaTip:  the pitching angle at the blade tip (in degrees)
        pitchFrontK: multiplier coefficient for pitch of the front propeller (4382)
        pitchRearK: multiplier coefficient for pitch of the rear propeller (4381)
        '''

        # r/R, CordLength/D, Pitch/D, skew(deg), Rake/D, MaxThick/C, MaxCamber/C
        dat4381 = np.array([
            [0.20, 0.1740, 1.3320, 0, 0, 0.2494, 0.0351],
            [0.25, 0.2020, 1.3380, 0, 0, 0.1960, 0.0369],
            [0.30, 0.2290, 1.3450, 0, 0, 0.1563, 0.0368],
            [0.40, 0.2750, 1.3580, 0, 0, 0.1069, 0.0348],
            [0.50, 0.3120, 1.3360, 0, 0, 0.0769, 0.0307],
            [0.60, 0.3370, 1.2800, 0, 0, 0.0567, 0.0245],
            [0.70, 0.3470, 1.2100, 0, 0, 0.0421, 0.0191],
            [0.80, 0.3340, 1.1370, 0, 0, 0.0314, 0.0148],
            [0.90, 0.2800, 1.0660, 0, 0, 0.0239, 0.0123],
            [0.95, 0.2100, 1.0310, 0, 0, 0.0229, 0.0128],
            [1.00, 0.0010, 0.9950, 0, 0, 0.0160, 0.0123]])
        dat4382 = np.array([
            [0.20, 0.1740, 1.4550, 0.0000, 0, 0.2494, 0.0430],
            [0.25, 0.2020, 1.4440, 2.3280, 0, 0.1960, 0.0395],
            [0.30, 0.2290, 1.4330, 4.6550, 0, 0.1563, 0.0370],
            [0.40, 0.2750, 1.4120, 9.3630, 0, 0.1069, 0.0344],
            [0.50, 0.3120, 1.3610, 13.948, 0, 0.0769, 0.0305],
            [0.60, 0.3370, 1.2850, 18.378, 0, 0.0567, 0.0247],
            [0.70, 0.3470, 1.2000, 22.747, 0, 0.0421, 0.0199],
            [0.80, 0.3340, 1.1120, 27.145, 0, 0.0314, 0.0161],
            [0.90, 0.2800, 1.0270, 31.575, 0, 0.0239, 0.0134],
            [0.95, 0.2100, 0.9850, 33.788, 0, 0.0229, 0.0140],
            [1.00, 0.0010, 0.9420, 36.000, 0, 0.0160, 0.0134]
        ])
        dat4383 = np.array([
            [0.20, 0.1740, 1.5660, 0.0000, 0, 0.2494, 0.0402],
            [0.25, 0.2020, 1.5390, 4.6470, 0, 0.1960, 0.0408],
            [0.30, 0.2290, 1.5120, 9.2930, 0, 0.1563, 0.0407],
            [0.40, 0.2750, 1.4590, 18.816, 0, 0.1069, 0.0385],
            [0.50, 0.3120, 1.3860, 27.991, 0, 0.0769, 0.0342],
            [0.60, 0.3370, 1.2960, 36.770, 0, 0.0567, 0.0281],
            [0.70, 0.3470, 1.1980, 45.453, 0, 0.0421, 0.0230],
            [0.80, 0.3340, 1.0960, 54.245, 0, 0.0314, 0.0189],
            [0.90, 0.2800, 0.9960, 63.102, 0, 0.0239, 0.0159],
            [0.95, 0.2100, 0.9450, 67.531, 0, 0.0229, 0.0168],
            [1.00, 0.0010, 0.8950, 72.000, 0, 0.0160, 0.0159]
        ])

        r2R = np.array([0.2, 0.25, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.9, 0.925, 0.95, 0.97, 0.98, 0.99, 0.998, 1.0])

        xm2D = (1 - ((r2R - 0.2)/0.8)**2)**0.5 * xdis / 2 # no rake, thus using elliptical
        xm2D = np.hstack((xm2D, -xm2D[-2::-1]))

        coef = [max((36-angledif)/36.0, 0), 1-abs(angledif-36)/36.0, max((angledif-36)/36.0, 0)]
        skew1 = dat4381[:,3]*coef[0] + dat4382[:,3]*coef[1] + dat4383[:,3]*coef[2]
        p2d1 = dat4381[:,2]*coef[0] + dat4382[:,2]*coef[1] + dat4383[:,2]*coef[2]
        f2c1 = dat4381[:,6]*coef[0] + dat4382[:,6]*coef[1] + dat4383[:,6]*coef[2]

        thetam = -itp.interp1d(dat4382[:,0], skew1, kind="quadratic")(r2R) # skew of 4382
        thetam_Rear = np.ones_like(r2R) * thetam[-1]
        thetam = np.hstack((thetam, thetam_Rear[-2::-1]))

        newChord = cls.outline_modify(ktip = ktip, kall = kall)
        chord2D = itp.interp1d(newChord[:,0], newChord[:,1], kind="cubic")(r2R)

        tipEnd = -2
        p2d1 = p2d1 * pitchFrontK
        pitch2D = itp.interp1d(dat4382[:,0], p2d1, kind="cubic")(r2R)
        PD_Rear = dat4381[:tipEnd, 2] * pitchRearK # one item less, because 0.95 will be deleted
        PD_Rear[-1] = p2d1[-1] # hear is at r/R = 1
        r_Rear = dat4381[:tipEnd,0]
        r_Rear[-1] = 1
        pitch2D_Rear = itp.interp1d(r_Rear, PD_Rear, kind="cubic")(r2R)
        pitch2D = np.hstack((pitch2D, pitch2D_Rear[-2::-1]))

        thick2c = itp.interp1d(dat4382[:,0], dat4382[:,5], kind="cubic")(r2R)
        thick2c = np.hstack((thick2c, thick2c[-2::-1]))

        camber2c = itp.interp1d(dat4382[:,0], f2c1*dat4382[:,1], kind="cubic")(r2R)/chord2D
        camber_Rear = dat4381[:tipEnd, 6] * dat4381[:tipEnd, 1] # one item less, because 0.95 will be deleted
        camber_Rear[-1] = f2c1[-1] * dat4382[-1,1] # hear is at r/R = 1
        camber2c_Rear = itp.interp1d(r_Rear, camber_Rear, kind="cubic")(r2R)/chord2D
        camber2c_Rear *= -1 # should be opposite on the rear blade
        camber2c = np.hstack((camber2c, camber2c_Rear[-2::-1]))

        r2R = np.hstack((r2R, r2R[-2::-1]))
        chord2D = np.hstack((chord2D, chord2D[-2::-1]))

        gamma, phi = toroidalBladeGeometry.calculatePitchingAndRoll(
            r2R, xm2D, thetam, pitch2D, gammaTip = gammaTip, influenceRange = 0.1)

        rake2D = None
        skew = None

        return r2R, xm2D, thetam, gamma, phi, chord2D, pitch2D, thick2c, camber2c, rake2D, skew

    def createpropeller(self, params):

        D = params['Diameter']
        Pa2Pf = params['Pa2Pf']
        gammaTip = params['gamma_tip']
        xdis = params['i_root/D']
        angledif = params['theta_root']

        tb = toroidalBladeGeometry()
        (r2R, xm2D, thetam, gamma, phi, chord2D, pitch2D, thick2c, camber2c,
        rake2D, skew) = self.combineParameter(
            ktip = 0.8, kall = 0.86,
            gammaTip = gammaTip,
            pitchFrontK = 1.0,
            pitchRearK = Pa2Pf,
            xdis = xdis,
            angledif = angledif
        )
        tb.fromDesignParameters(r2R, xm2D, thetam, gamma, phi, chord2D, pitch2D, thick2c, camber2c, D)
        
        coordX = np.array(tb.Xp)
        coordY = np.array(tb.Yp)
        coordZ = np.array(tb.Zp)
        self.__xg = coordX
        self.__yg = coordY
        self.__zg = coordZ
        self.__Z = params['noBlade']

    def _create_vtk(self):
        '''
        Create a vtk unstructured grid with given coordinates.

        Parameters:
        ==========
        Z: number of propeller blades
        '''
        import vtkmodules.all as vtk

        Z = self.__Z
        coordX = np.array(self.__xg)
        coordY = np.array(self.__yg)
        coordZ = np.array(self.__zg)
        nrow = coordX.shape[0]
        ncol = coordX.shape[1]
        nBladePoint = nrow * ncol
        ugrid = vtk.vtkUnstructuredGrid()
        points = vtk.vtkPoints()
        for iz in range(Z):
            theta = np.pi * 2 / Z * iz
            cy = coordY * np.cos(theta) - coordZ * np.sin(theta)
            cz = coordY * np.sin(theta) + coordZ * np.cos(theta)
            for ir in range(nrow):
                for ic in range(ncol):
                    points.InsertNextPoint(
                        coordX[ir, ic], cy[ir, ic], cz[ir, ic]
                        )

        for iz in range(Z):
            base = nBladePoint * iz
            for ir in range(nrow-1):
                for ic in range(ncol-1):
                    ids = [ ir     * ncol + ic   + base,
                            (ir+1) * ncol + ic   + base,
                            (ir+1) * ncol + ic+1 + base,
                            ir     * ncol + ic+1 + base]
                    ugrid.InsertNextCell(vtk.VTK_POLYGON, 4, ids)

        ugrid.SetPoints(points)
        return ugrid

    def saveVTK(self, filePath):
        from vtk import vtkXMLUnstructuredGridWriter
        ugrid = self._create_vtk()
        writer = vtkXMLUnstructuredGridWriter()
        writer.SetFileName(filePath)
        writer.SetDataModeToAppended()
        writer.SetCompressorTypeToNone()
        writer.SetInputData(ugrid)
        writer.Write()

    def save3DCoordinates(self, filePath):
        Xp = np.array(self.__xg)
        Yp = np.array(self.__yg)
        Zp = np.array(self.__zg)
        nr = Xp.shape[0]
        with open(filePath, "w") as file:
            for ir in range(nr):
                file.write(f"ROW{ir+1},\n")
                for i in range(0, len(Xp[ir])):
                    file.write(f"{Xp[ir,i]}, {Yp[ir,i]}, {Zp[ir,i]}\n")

if __name__ == "__main__":
    params = {
        'Diameter' : 250,
        'Pa2Pf' : 1.0,
        'gamma_tip' : 0.0, # tip pitching angle in degree
        'i_root/D' : 0.2, # root axial spacing
        'theta_root' : 0, # root phase angle
        'noBlade' : 3, # number of blades
    }

    pg = paperGeom()
    pg.createpropeller(params)
    pg.save3DCoordinates("./toroidalBladeCoordinates.dat")

    # the following function needs the installation of vtk
    pg.saveVTK("./toroidalPropeller.vtu")